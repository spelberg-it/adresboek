# Adresboek

Adresboek voor Scrum/Git

Extra regel
Regel 2 is extra

## Git

Eerste keer git setup:

    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com

    
## MySql driver
    
Download MariaDB JDBC driver van

<https://downloads.mariadb.com/Connectors/java/connector-java-2.0.2/mariadb-java-client-2.0.2.jar> 

en zet deze in de ```lib``` directory.


## MySql database

Aanmaken van database en user gaat met het script in

    sql/dbinit.sql
    

## Java 8

Java 8 optimalisaties doorgevoerd
